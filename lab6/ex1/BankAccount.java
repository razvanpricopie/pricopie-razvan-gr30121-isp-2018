package pricopie.razvan.lab6.ex1;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner,double balance){
        this.owner=owner;
        this.balance=balance;
    }

    public String getOwner() {
        return owner;
    }

    public void withdraw(double amount){
        this.balance=this.balance-amount;
    }

    public void deposit(double amount){
        this.balance=this.balance+amount;
    }


    public boolean equals(Object obj){
        if(obj instanceof BankAccount){
            BankAccount b=(BankAccount) obj;
            return balance==b.balance && b.owner.equals(owner);
        }
        return false;
    }

    public int hashCode(){
        return (int)balance+owner.hashCode();
    }

}

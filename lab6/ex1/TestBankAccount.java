package pricopie.razvan.lab6.ex1;

public class TestBankAccount {
    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Razvan", 200);
        BankAccount b2 = new BankAccount("Razvan", 200);
        BankAccount b3 = new BankAccount("Andrei", 50);
        BankAccount b4 = new BankAccount("Paul", 50);

        if (b1.equals(b2)) {
            System.out.println(b1.getOwner()+" and "+b2.getOwner()+" are equals");
        }else System.out.println(b1.getOwner()+" and "+b2.getOwner()+" are not equals");
        if (b3.equals(b4)) {
            System.out.println(b3.getOwner()+" and "+b4.getOwner()+" are equals");
        }else System.out.println(b3.getOwner()+" and "+b4.getOwner()+" are not equals");

        System.out.println(b1.hashCode());
        System.out.println(b3.hashCode());
        System.out.println(b4.hashCode());

    }
}

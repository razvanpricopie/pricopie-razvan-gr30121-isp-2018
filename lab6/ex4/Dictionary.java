package pricopie.razvan.lab6.ex4;

import java.util.*;
import static java.lang.System.*;

public class Dictionary {
    HashMap h = new HashMap();

    public void addWord(Word w, Definition d) {
        if (h.containsKey(w))
            out.println("Modify existing word!");
        else
            out.println("Add new word.");
        h.put(w, d);
    }

    public Object getDefinition(Word w) {
        out.println("Search " + w);
        out.println(h.containsKey(w));
        return h.get(w);
    }

    public void listDictionar() {
        out.println(h);
    }

    public void getAllWords() {

        for (Object word : h.keySet()) System.out.println(word.toString());
    }

    public void getAllDefinitions() {

        for (Object definition : h.values()) System.out.println(definition.toString());
    }
}
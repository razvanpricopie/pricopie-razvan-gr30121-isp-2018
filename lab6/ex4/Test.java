package pricopie.razvan.lab6.ex4;

import java.io.*;

public class Test {
    public static void main(String[] args) throws IOException {

        Dictionary d = new Dictionary();
        char answer;
        String line;
        String explic;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("Menu");
            System.out.println("a - Add word");
            System.out.println("s - Search word");
            System.out.println("w - List words");
            System.out.println("d - List def");
            System.out.println("l - List dictionary");
            System.out.println("e - Exit");

            line = fluxIn.readLine();
            answer = line.charAt(0);

            switch (answer) {
                case 'a':
                case 'A':
                    System.out.println("Enter the word:");
                    line = fluxIn.readLine();
                    if (line.length() > 1) {
                        System.out.println("Enter the def:");
                        explic = fluxIn.readLine();
                        d.addWord(new Word(line), new Definition(explic));
                    }
                    break;
                case 'c':
                case 'C':
                    System.out.println("Search word:");
                    line = fluxIn.readLine();
                    if (line.length() > 1) {
                        Word x = new Word(line);
                        explic = String.valueOf(d.getDefinition(x));

                        if (explic == null)
                            System.out.println("The word could not be found!");
                        else
                            System.out.println("Definition:" + explic);
                    }
                    break;
                case 'l':
                case 'L':
                    System.out.println("List:");
                    d.listDictionar();
                    break;
                case 'w':
                case 'W':
                    System.out.println("Get all words");
                    d.getAllWords();
                    break;
                case 'd':
                case 'D':
                    System.out.println("Get all definitions");
                    d.getAllDefinitions();
                    break;

            }
        } while (answer != 'e' && answer != 'E');
        System.out.println("Over!");


    }
}

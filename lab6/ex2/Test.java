package pricopie.razvan.lab6.ex2;

public class Test {

    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Razvan",300);
        bank.addAccount("Paul",60);
        bank.addAccount("Andrei",40);
        bank.addAccount("Miron",100);
        bank.addAccount("Cosmin",90);
        bank.addAccount("Ioana",60);
        System.out.println("Print Accounts by balance");
        bank.printAccounts();
        System.out.println("Print Accounts between limits");
        bank.printAccounts(40,60);
        System.out.println("Get Account by owner name");
        bank.getAccount("Razvan");
        System.out.println("Get All Account order by name");
        bank.getAllAccount();

    }
}

package pricopie.razvan.lab1.e5;

import java.util.Random;

public class BubbleSort {
    public static void main(String[] args) {
        int aux;
        int[] v=new int[10];
        Random random = new Random();

        for(int i=0;i<10;i++){
            v[i]=random.nextInt(100);
            System.out.print("\nElementul "+(i+1)+" : "+v[i]);
        }

        for(int i=0;i<9;i++){
            for(int j=0;j<9-i;j++){
                if(v[j]>v[j+1]){
                    aux=v[j];
                    v[j]=v[j+1];
                    v[j+1]=aux;
                }
            }
        }
        System.out.print("\nVectorul sortat este: ");
        for(int i=0;i<10;i++) System.out.print(v[i]+" ");
    }
}

package pricopie.razvan.lab1.e1;

import java.util.Scanner;

public class Maxim {
    public static void main(String[] args) {
        System.out.println("Introduceti cele 2 numere: ");
        Scanner in = new Scanner(System.in);
        System.out.print("x= ");
        int x = in.nextInt();
        System.out.print("y= ");
        int y = in.nextInt();

        System.out.println("Maximul este: " + Math.max(x, y));
    }
}

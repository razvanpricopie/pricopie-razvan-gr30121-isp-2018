package pricopie.razvan.lab1.e6;

import java.util.Scanner;

public class Factorial1 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Introduceti numarul pentru care se calculeaza factorialul: ");
        int n = in.nextInt();
        int factorial=1;

        for(int i=n;i>0;i--){
            if(n!=0) {
                factorial *= i;
            }else factorial=1;
        }

        System.out.println("Factorialul lui "+n+" este "+factorial);
    }
}

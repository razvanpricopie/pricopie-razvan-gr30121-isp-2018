package pricopie.razvan.lab1.e6;

import java.util.Scanner;

public class Factorial2 {
    static int fact(int n){
        if(n==0) return 1;
        else
            return (n*fact(n-1));
    }

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.print("Introduceti numarul pentru care se calculeaza factorialul: ");
        int n = in.nextInt();

        System.out.println("Factorialul lui "+n+" este "+fact(n));
    }
}

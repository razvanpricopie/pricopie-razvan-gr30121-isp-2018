package pricopie.razvan.lab1.e4;

import java.util.Scanner;

public class Max_vector {
    public static void main(String[] args) {
        int aux;
        Scanner in = new Scanner(System.in);

        System.out.print("Introduceti numarul de elemente din vector: ");
        int n = in.nextInt();
        int[] v = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.println("Introduceti elementul "+(i+1));
            v[i] = in.nextInt();
        }

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (v[i] > v[j]) {
                    aux = v[j];
                    v[j] = v[i];
                    v[i] = aux;
                }
            }
        }

        for (int i = 0; i < n; i++) {
            System.out.println(v[i]);
        }


        System.out.println("Minimul este " + v[0]);
        System.out.println("Maximul este: " + v[n - 1]);

    }
}

package pricopie.razvan.lab1.e3;

import java.util.Scanner;

public class PrimeNumber {
    public static void main(String[] args) {

        int ok = 0, nr_total = 0;

        Scanner in = new Scanner(System.in);
        System.out.print("Introduceti limita inferioara: ");
        int a = in.nextInt();
        System.out.print("Introduceti limita superioara: ");
        int b = in.nextInt();

        if (a > b) System.out.println("Limita inferioara trebuie sa fie mai mica!!!");
        else {
            for (int i = a; i <= b; i++) {
                for (int j = 2; j < i; j++) {
                    if(i==2) System.out.print("2 ");
                    if (i % j == 0) {
                        ok = 0;
                        break;

                    }else {
                        ok = 1;
                        nr_total++;
                    }
                }
                if (ok == 1) System.out.print(i + " ");
            }
            System.out.println("\nNumarul total de numere prime dintre " + a + " si " + b + " este: " + nr_total);
        }
    }
}




package pricopie.razvan.lab1.e7;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {
    public static void main(String[] args){

        Random random = new Random();
        Scanner in = new Scanner(System.in);

        int number=random.nextInt(10);
        int counter=0;
        System.out.print("Introduceti numarul: ");
        int n = in.nextInt();


        while(n!=number && counter !=2){
            counter++;
            if(n>number){
                System.out.println("Wrong answer,your number is too high!");
                System.out.println("Introduceti numarul: ");
                n = in.nextInt();
            }else if(n<number){
                System.out.println("Wrong answer,your number is too low!");
                System.out.println("Introduceti numarul: ");
                n = in.nextInt();
            }
            else if (n==number) System.out.println("Correct answer!");
        }
        System.out.println("The number is: "+number);
    }
}

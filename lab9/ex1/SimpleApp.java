package pricopie.razvan.lab9.ex1;

import javax.swing.JFrame;
import java.awt.*;

public class SimpleApp extends JFrame {

    public SimpleApp() {
        setTitle("Titlul ferestrei");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,600);
        setVisible(true);
    }

    public static void main(String[] args) {
        SimpleApp a = new SimpleApp();
    }
}

package pricopie.razvan.lab3.e2;

public class TestCircle {
    public static void main(String[] args){
        Circle C1=new Circle();
        Circle C2=new Circle(5.27);
        Circle C3=new Circle("black");

        System.out.println(C1.getRadius());
        System.out.println(C2.getRadius());
        System.out.println(C3.getRadius());

        System.out.println(C1.getArea());
        System.out.println(C2.getArea());
        System.out.println(C3.getArea());


    }
}

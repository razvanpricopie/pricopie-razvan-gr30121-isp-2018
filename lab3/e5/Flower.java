package pricopie.razvan.lab3.e5;

    /*
    Modify Flower class from Ciclul de viaţă al obiectelor so that it keeps track of the number of constructed object.
    Add a method which returns the number of constructed objects.
    Use 'static' variables and methods for implementing this task.
    */

public class Flower {
    int petal;
    static int nr=0;

    Flower() {
        System.out.println("Flower has been created!");
        nr++;
    }

    public static int nr_obj(){
        return nr;
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for (int i = 0; i < 5; i++) {
            Flower f = new Flower();
            garden[i] = f;
        }
        System.out.println("Number of objects created: "+nr_obj());
    }
}

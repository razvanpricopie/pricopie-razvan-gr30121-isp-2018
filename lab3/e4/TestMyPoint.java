package pricopie.razvan.lab3.e4;

public class TestMyPoint {
    public static void main(String[] args){
        MyPoint p1=new MyPoint();
        MyPoint p2=new MyPoint(2,5);

        System.out.println(p1.getX()+" "+p1.getY());
        System.out.println(p2.getX()+" "+p2.getY());
        p1.setX(5);
        p1.setY(7);
        System.out.println(p1.getX()+" "+p1.getY());
        p2.setXY(3,3);
        System.out.println(p2.getX()+" "+p2.getY());
        System.out.println("Distance is: "+p1.distance(2,3));
        System.out.println("Second distance is: "+p1.distance(p2));
    }
}

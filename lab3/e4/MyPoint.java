package pricopie.razvan.lab3.e4;
import java.lang.Math;
/*A class called MyPoint, which models a 2D point with x and y coordinates contains:

        Two instance variables x (int) and y (int).
        A “no-argument” (or “no-arg”) constructor that construct a point at (0, 0).
        A constructor that constructs a point with the given x and y coordinates.
        Getter and setter for the instance variables x and y.
        A method setXY() to set both x and y.
        A toString() method that returns a string description of the instance in the format “(x, y)”.
        A method called distance(int x, int y) that returns the distance from this point to another point at the given (x, y) coordinates.
        An overloaded distance(MyPoint another) that returns the distance from this point to the given MyPoint instance another.
        Write the code for the class MyPoint. Also write a test class (called TestMyPoint) to test all the methods defined in the class.*/

public class MyPoint {
        int x,y;

    MyPoint(){
        this.x=0;
        this.y=0;
    }

    MyPoint(int a,int b){
        this.x=a;
        this.y=b;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int a, int b){
        this.x=a;
        this.y=b;
    }

    public String toString(){
        return "("+this.x+"," +this.y+")";
    }

    public double distance(int x, int y){
        return Math.sqrt(Math.pow((x-this.x),2)+Math.pow((y-this.y),2));
    }

    public double distance(MyPoint a){
        return Math.sqrt(Math.pow((a.x-this.x),2)+Math.pow((a.y-this.y),2));
    }
}

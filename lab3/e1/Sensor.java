package pricopie.razvan.lab3.e1;

/*A class Sensor contains:

One instance variable 'value' (of type int);
One default constructor which initialize the value to -1;
One change(int k) method which add to the current sensor value k;
One toString() method which returns the current value of the sensor;
Write a class which models the class Sensor . Write a class TestSensor which test Sensor class.*/

public class Sensor {
    private int value;

    public Sensor(){
        value=-1;
    }

    public void change(int k){
        value+=k;
    }

    public int get(){
        return value;
    }
}

package pricopie.razvan.lab3.e1;

public class TestSensor {
    public static void main(String[] args){
        Sensor s1=new Sensor();
        Sensor s2=new Sensor();

        s1.change(30);
        s2.change(2);
        System.out.println(s1.get());
        System.out.println(s2.get());
    }
}

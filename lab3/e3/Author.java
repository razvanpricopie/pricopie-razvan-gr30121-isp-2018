package pricopie.razvan.lab3.e3;

/*A class called Author contains:

        Three private instance variables: name (String), email (String), and gender (char of either 'm' or 'f');
        One constructor to initialize the name, email and gender with the given values;
        public Author (String name, String email, char gender) {……}
        (There is no default constructor for Author, as there are no defaults for name, email and gender.)
        public getters/setters: getName(), getEmail(), setEmail(), and getGender();
        (There are no setters for name and gender, as these attributes cannot be changed.)
        A toString() method that returns “author-name (gender) at email”, e.g., “My Name (m) at myemail@somewhere.com”.
        Write the Author class. Also write a test program called TestAuthor to test the constructor and public methods.*/

public class Author {
    private String name, email;
    private char gender;

    Author(String name, String email, char gender) {
        if (Character.toLowerCase(gender) == 'm' || Character.toLowerCase(gender) == 'f') {
            this.gender = gender;
        } else System.out.println("Gender not accepted");
        this.name = name;
        this.email = email;
    }

    public String getName(){
        return this.name;
    }

    public String getEmail() {
        return this.email;
    }

    public char getGender() {
        return this.gender;
    }


    public void show() {
        System.out.println(getName() + "(" + gender + ") at: " + getEmail());
    }
}

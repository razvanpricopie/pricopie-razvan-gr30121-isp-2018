package pricopie.razvan.lab7.ex2;

import java.io.*;
import java.util.Scanner;

public class CountLetter {
    public static void main(String[] args) throws IOException {
        File myFile=new File("C:\\Users\\Razvan\\Desktop\\Semestru 2\\ISP\\Laburi_ISP\\src\\pricopie\\razvan\\lab7\\ex2\\date.txt");
        String line = "";
        Scanner scanner = new Scanner(new FileReader(myFile));
        int counter = 0;

        System.out.println("Introduceti litera: ");
        Scanner in = new Scanner(System.in);
        char character = in.next().charAt(0);

        try {
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();


                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) == character) counter++;
                }
            }
            System.out.println("Caracterul "+character+" a aparut de "+counter);

        } finally {
            scanner.close();
        }
    }
}


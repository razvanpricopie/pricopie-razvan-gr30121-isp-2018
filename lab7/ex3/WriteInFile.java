package pricopie.razvan.lab7.ex3;

import java.io.*;
import java.util.Scanner;

public class WriteInFile {
    public void write() throws Exception{
        try{
            File file=new File("char.txt");
            PrintWriter write=new PrintWriter("char.txt");
            Scanner citire = new Scanner (System.in);
            System.out.println ("Enter text: ");
            String l = citire.nextLine ();
            write.print ();
            write.close ();
        }
        catch (FileNotFoundException e) {
            System.out.println ("Invalid");
        }
    }
}

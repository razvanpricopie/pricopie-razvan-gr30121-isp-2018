package pricopie.razvan.lab7.ex1;

public class NumberExcep extends Exception {
    int n;

    public NumberExcep(int n, String msg){
        super(msg);
        this.n=n;
    }

    public int getNumber() {
        return n;
    }
}

package pricopie.razvan.lab7.ex1;

public class Cofee {
    private int temp;
    private int conc;
    private int number;

    Cofee(int t, int c,int n) {
        this.temp = t;
        this.conc = c;
        this.number=n;
    }

    public Cofee(int t, int c) {
    }

    int getTemp() {
        return temp;
    }

    int getConc() {
        return conc;
    }

    public int getNumber() {
        return number;
    }

    public String toString() {
        return "[cofee temperature=" + temp + ", concentration=" + conc + ", number of coffes= "+number+" ]";
    }
}

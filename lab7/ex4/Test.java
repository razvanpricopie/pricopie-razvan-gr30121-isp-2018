package pricopie.razvan.lab7.ex4;

import java.io.*;
import java.util.Scanner;

public class Test {
    public static void main (String[] args) throws Exception {
        String filename;
        Car a = new Car ();
        Scanner citire = new Scanner (System.in);
        int in = -1;
        while (in != 6) {
            System.out.println ("\n1)Add car\n2)Show car\n3)Save Car Objects\n4)View Existing Car Objects\n5)Read and Display\n6)Exit\n");
            System.out.println ("Choose the option: ");
            in = citire.nextInt ();
            switch (in) {
                case 1:
                    a.add_Car ();
                    break;
                case 2:
                    System.out.println (a.show_Car ());
                    break;
                case 3:
                    FileOutputStream fin = null;
                    ObjectOutputStream out = null;
                    try {
                        System.out.println ("Save with the next name(.ser) :");
                        filename = citire.next ();
                        if (filename.endsWith (".ser")) {
                            fin = new FileOutputStream (filename);
                            out = new ObjectOutputStream (fin);
                            out.writeObject (a);
                            out.close ();
                        }
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }
                    break;
                case 4:
                    File folder = new File ("C:\\Users\\Razvan\\Desktop\\Semestru 2\\ISP\\Laburi_ISP");
                    File[] listOfFiles = folder.listFiles ();
                    for (File file : listOfFiles) {
                        if (file.getName ().endsWith (".ser")) {
                            System.out.print (" " + file.getName () + "   ");
                        }
                    }
                    break;
                case 5:
                    FileInputStream fis = null;
                    ObjectInputStream is = null;
                    try {
                        System.out.println ("Get the next filename(.ser): ");
                        filename = citire.next ();
                        if (filename.endsWith (".ser")) {
                            fis = new FileInputStream (filename);
                            is = new ObjectInputStream (fis);
                            a = (Car) is.readObject ();
                            is.close();
                            System.out.println (a);
                        }
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }
                    break;
                case 6:
                    break;
                default:
                    System.out.println ("Invalid");
            }
        }

    }
}

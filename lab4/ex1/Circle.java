package pricopie.razvan.lab4.ex1;

public class Circle {
    private double radius;
    private String color;

    Circle() {
        this.radius = 1.0;
        this.color = "red";
    }

    Circle(double r) {
        radius = r;
    }

    Circle(String c) {
        color = c;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    public String toString(){
        return "Color is: "+color+" and radius is: "+radius;
    }
}

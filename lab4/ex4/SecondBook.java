package pricopie.razvan.lab4.ex4;

import pricopie.razvan.lab4.ex2.Author;

public class SecondBook {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock=0;

    public SecondBook(String name,Author[] authors,double price){
        this.name=name;
        this.authors=authors;
        this.price=price;
    }

    public SecondBook(String name, Author[] authors, double price, int qtyInStock) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public void printAuhors(){
        for(Author a:this.authors){
            System.out.println(a.getName());
        }
    }

    public String toString(){
        return getName()+" by "+ authors.length+" authors.";
    }
}

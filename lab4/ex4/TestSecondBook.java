package pricopie.razvan.lab4.ex4;

import pricopie.razvan.lab4.ex2.Author;

public class TestSecondBook {
    public static void main(String[] args) {
        Author[] authori = new Author[2];
        authori[0] = new Author("Razvan", "mail@gmailcom", 'm');
        authori[1] = new Author("Ion", "crenguta@gmailcom", 'm');

        SecondBook b1 = new SecondBook("Harap-Alb", authori, 4.6, 9);

        b1.printAuhors();
        System.out.println(b1);
    }
}

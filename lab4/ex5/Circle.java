package pricopie.razvan.lab4.ex5;

public class Circle {
    private double radius;
    private String color;

    Circle() {
        this.radius = 1.0;
        this.color = "red";
    }

    Circle(double r) {
        radius = r;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    public String toString(){
        return "Color is: "+color+" and radius is: "+radius;
    }
}

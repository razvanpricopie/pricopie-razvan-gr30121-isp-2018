package pricopie.razvan.lab4.ex5;

public class Cylinder extends Circle{
    private double height;

    public Cylinder(){
        height=1.0;
    }

    public Cylinder(double radius){
        super(radius);
    }

    public Cylinder(double radius, double height){
        super(radius);
        this.height=height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume(){
        return getRadius()*getRadius()*Math.PI*getHeight();
    }

    public String toString(){
        return "For this Cylinder radius is: "+getRadius()+", height is: "+getHeight()+" and volume is: "+getVolume();
    }


}

package pricopie.razvan.lab4.ex5;

public class TestCylinder {
    public static void main(String[] args){
        Cylinder c1=new Cylinder();
        Cylinder c2=new Cylinder(9);
        Cylinder c3=new Cylinder(9,16);

        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);

    }
}

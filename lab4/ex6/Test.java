package pricopie.razvan.lab4.ex6;

public class Test {
    public static void main(String[] args){
        Shape s1=new Shape();
        Shape s2=new Shape("black",false);

        s1.tooString();
        s2.tooString();

        Circle c1=new Circle();
        Circle c2=new Circle(4.2);
        Circle c3=new Circle(6.1,"pink",true);

        c1.toooString();
        c2.toooString();
        c3.toooString();

        Rectangle r1=new Rectangle();
        Rectangle r2=new Rectangle(4,5);
        Rectangle r3=new Rectangle(12,3,"white",true);

        r1.toStringR();
        r2.toStringR();
        r3.toStringR();
        Square sq1=new Square();
        Square sq2=new Square(4);
        Square sq3=new Square(2,"green",false);

        sq1.toStringS();
        sq2.toStringS();
        sq3.toStringS();
    }
}

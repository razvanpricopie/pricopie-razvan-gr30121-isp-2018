package pricopie.razvan.lab4.ex2;

import java.sql.SQLOutput;

public class Author {
        private String name, email;
        private char gender;

        public Author(String name, String email, char gender){
            if (Character.toLowerCase(gender) == 'm' || Character.toLowerCase(gender) == 'f') {
                this.gender = gender;
            } else System.out.println("Gender not accepted");
            this.name = name;
            this.email = email;
        }

        public String getName () {
            return name;
        }

        public String getEmail () {
            return email;
        }

        public void setEmail (String email){
            this.email = email;
        }

        public char getGender () {
            return gender;
        }

        public String toString () {
            return getName() + " (" + getGender() + ") at " + getEmail();
        }

}

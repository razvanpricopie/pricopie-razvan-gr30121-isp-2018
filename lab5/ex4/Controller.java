package pricopie.razvan.lab5.ex4;

import pricopie.razvan.lab5.ex3.*;
public class Controller {
    private static Controller control;

    private Controller() {
    }

    public static Controller getInstance() {
        if (control == null) {
            control = new Controller();
            System.out.println("NEW");
        }
        else
            System.out.println("OLD");
        return control;
    }

    public void control() throws InterruptedException {
        Sensor t = new TemperatureSensor();
        Sensor l = new LightSensor();

        int sec = 1;
        while (sec <= 20) {
            System.out.println("Temp: " + t.readValue());
            System.out.println("Light: " + l.readValue());
            System.out.println("Sec: " + sec);
            sec++;
            Thread.sleep(1000);
        }
    }
}

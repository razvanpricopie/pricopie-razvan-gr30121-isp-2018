package pricopie.razvan.lab5.ex1;

public class Square extends Rectangle {

    Square(){

    }

    Square(double side){
        this.setLength(side);
        this.setWidth(side);
    }

    Square(double side,String color,boolean filled){
        this.isFilled();
        this.setWidth(side);
        this.setWidth(side);
    }

    public double getSide(){
        return this.getLength();
    }

    public void setSide(double side){
        this.setWidth(side);
        this.setLength(side);
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

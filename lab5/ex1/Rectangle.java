package pricopie.razvan.lab5.ex1;

public class Rectangle extends Shape {
    double width,length;

    Rectangle(){

    }

    Rectangle(double width,double length){
        this.width=width;
        this.length=length;
    }

    Rectangle(double width,double length,String color,boolean filled){
        super(color,filled);
        this.width=width;
        this.length=length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        return this.width*this.length;
    }

    public double getPerimeter(){
        return 2*this.width+2*this.length;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

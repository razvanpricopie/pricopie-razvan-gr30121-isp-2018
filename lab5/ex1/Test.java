package pricopie.razvan.lab5.ex1;

public class Test {
    public static void main(String[] args){
        Circle c1=new Circle();
        Circle c2=new Circle(1.3);
        Circle c3=new Circle(2,"black",true);

        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);

        Rectangle r1=new Rectangle();
        Rectangle r2=new Rectangle(4,2);
        Rectangle r3=new Rectangle(8,3,"red",false);

        System.out.println(r1);
        System.out.println(r2);
        System.out.println(r3);

        Square s1=new Square();
        Square s2=new Square(3);
        Square s3=new Square(4,"white",true);

        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);


    }
}

package pricopie.razvan.lab5.ex1;

public class Circle extends Shape {
    double radius;

    Circle(){

    }

    Circle(double radius){
        this.radius=radius;
    }

    Circle(double radius,String color,boolean filled){
        super(color,filled);
        this.radius=radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return Math.PI*Math.pow(this.radius,2);
    }

    public double getPerimeter(){
        return 2*Math.PI*this.radius;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
